package main

import (
	"strings"
)

type registerCustomGroupByFunc map[string]func(*Item, ItemsGroupedBy)

var RegisterGroupByCustom registerCustomGroupByFunc

func (r registerCustomGroupByFunc) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

func init() {
	// RegisterGroupByCustom = make(registerCustomGroupByFunc)
	// RegisterGroupByCustom["gebruiksdoelen-mixed"] = GroupByGettersGebruiksdoelen
	// RegisterGroupByCustom["postcodehuisnummer"] = GroupByGettersPostcodehuisnummer
}
