/*
Test some basic request handling touches most functionality

-typeahead,
-list
*/
package main

import (
	"encoding/json"
	"fmt"

	// "io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	. "github.com/Attumm/settingo/settingo"
)

var handler http.Handler

/* load some data 9 records*/
func TestMain(m *testing.M) {

	defaultSettings()

	SETTINGS.Set(
		"csv", "./testdata/items.csv",
		"test dataset")

	SETTINGS.Set("delimiter", ";", "delimiter")

	SETTINGS.Set("channelwait", "0.02s", "timeout for channel loading")

	loadcsv(itemChan)
	close(itemChan)
	ItemChanWorker(itemChan)

	handler = setupHandler()

	BuildGeoIndex()

	// Run the test
	m.Run()
}

func TestCsvLoading(t *testing.T) {

	size := len(ITEMS)

	// we need minimal 10 for typeahead return.
	if size != 82 {
		t.Errorf("expected 10 ITEMS got %d", size)
	}
}

func TestBasicHandlers(t *testing.T) {

	if len(ITEMS) < 10 {
		t.Error("no items")
	}

	type testCase struct {
		url      string
		expected string
	}

	tests := []testCase{
		testCase{"/list/?search=1", "82"}, // matches everything.
		testCase{"/typeahead/huisnummer/?match-huisnummer=5", "1"},
		testCase{"/typeahead/pid/?search=041510000000733", "7"},
		testCase{"/typeahead/gebruiksdoelen/?contains=logiesfunctie", "3"},
		testCase{"/list/?contains-gebruiksdoelen=logiesfunctie", "1"},
		testCase{"/list/?contains-invalid=shouldgiveerror", "82"},
		testCase{"/list/?gte-huisnummer=5", "78"},
		testCase{"/list/?lte-huisnummer=5", "5"},

		testCase{"/help/", ""},
	}

	for i := range tests {
		req := httptest.NewRequest("GET", tests[i].url, nil)
		w := httptest.NewRecorder()
		handler.ServeHTTP(w, req)
		resp := w.Result()
		if resp.StatusCode != 200 {
			t.Errorf("request to %s failed", tests[i].url)
			t.Error(resp)
		}

		if tests[i].expected == "" {
			continue
		}

		if resp.Header.Get("Total-Items") != tests[i].expected {
			t.Errorf("total hits mismatch from %s %s != %s",
				tests[i].url,
				tests[i].expected,
				resp.Header.Get("Total-Items"),
			)
			t.Error(resp)
		}
	}
}

// 3 point CCWP Counter clockwise geometry fails.
func TestMinCCWGeoQuery(t *testing.T) {

	data := url.Values{}
	data.Set("groupby", "postcode")
	data.Set("reduce", "count")

	// bbox around houses 1121RM (41 - 37 ) ~ 5 houses.

	geojson := `
{
	"type": "Polygon",
	"coordinates": [
	    [

            [4.905418040441532,52.42751044670831],
            [4.905069353269198,52.427371427602225],
            [4.905452909158129,52.42725694095054],
            [4.905418040441532,52.42751044670831]

	    ]
	]
}
	`
	data.Set("geojson", geojson)

	params := strings.NewReader(data.Encode())

	req := httptest.NewRequest("POST", "/list/", params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	w := httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	// Read the response body

	resp := w.Result()
	defer resp.Body.Close()

	j := GroupByResult{}
	err := json.NewDecoder(resp.Body).Decode(&j)

	if err != nil {
		t.Error("json parsing of query body failed")
	}

	if resp.StatusCode != 200 {
		t.Errorf("geo request to %s failed statuscode", req.URL)
		t.Error(j)
	}

	headerQuery := resp.Header.Get("Query")
	query := Query{}

	err = json.Unmarshal([]byte(headerQuery), &query)
	if err != nil {
		t.Error("parsing of query header failed")
	}

	if query.GeometryGiven != true {
		t.Errorf("geo request to %s failed ", req.URL)
		t.Error(resp.Header.Get("Query"))
		// t.Error(resp.Header.Get("GeometryGiven"))
		t.Error(j)
		t.Error(query)
	}

	if resp.Header.Get("Total-Items") != "4" {
		t.Errorf("geo request count is not 4 but %s", resp.Header.Get("Total-Items"))
	}

}

// Test geojson queries combined with groupby and reduce.
func TestGeoQuery(t *testing.T) {

	// BuildGeoIndex()

	if len(ITEMS) < 10 {
		t.Error("no items")
	}

	if len(S2CELLS) == 0 {
		t.Error("geo indexing failed")
	}

	if len(S2CELLMAP) == 0 {
		t.Error("geo indexing failed")
	}

	data := url.Values{}
	data.Set("groupby", "postcode")
	data.Set("reduce", "count")

	// bbox around houses 1121RM (41 - 37 ) 5 houses.
	geojson := `
{
	"type": "Polygon",
	"coordinates": [
	    [
	    [ 4.905052689424139, 52.42748675750323 ],
            [ 4.905561261182783, 52.42729724588111 ],
            [ 4.905561261182783, 52.42748675750323 ],
            [ 4.905052689424139, 52.42748675750323 ]
	    ]
	]
}
	`
	data.Set("geojson", geojson)

	params := strings.NewReader(data.Encode())

	req := httptest.NewRequest("POST", "/list/", params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	w := httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	// Read the response body

	resp := w.Result()
	defer resp.Body.Close()

	j := GroupByResult{}
	err := json.NewDecoder(resp.Body).Decode(&j)

	if resp.StatusCode != 200 {
		t.Errorf("geo request to %s failed statuscode", req.URL)
		t.Error(j)
	}

	headerQuery := resp.Header.Get("Query")
	query := Query{}

	err = json.Unmarshal([]byte(headerQuery), &query)
	if err != nil {
		t.Error("parsing of query header failed")
	}

	if query.GeometryGiven != true {
		t.Errorf("geo request to %s failed ", req.URL)
		t.Error(resp.Header.Get("Query"))
		// t.Error(resp.Header.Get("GeometryGiven"))
		t.Error(j)
		t.Error(query)
	}

	if resp.Header.Get("Total-Items") != "5" {
		t.Errorf("geo request count is not 5 but %s", resp.Header.Get("Total-Items"))
	}

	if err != nil {
		t.Error(err)
	}

	// fmt.Println(j)

	if j["1121RM"]["count"] != "5" {
		t.Errorf("geo request json response count is not 1 but %s", j["1121RM"]["count"])
		q := fmt.Sprintf("%+v\n", query)
		t.Error(q)

	}
}

func TestInvalidGeoQuery(t *testing.T) {

	data := url.Values{}
	data.Set("groupby", "postcode")
	data.Set("reduce", "count")

	geojson := `
{
    "type": "Polygon",
    "coordinates": [
	    [
	    [4.90518244016161,52.42753296120998],
	    [4.905702788710585,52.42737922248702],
	    [4.9052307199241625,52.427217305645684],
	    [4.905638415694142,52.42735959623997],
	    [4.905622322440422,52.42741029737843],
	    [4.90518244016161,52.42753296120998]
	    ]
    ]
}`
	data.Set("geojson", geojson)

	params := strings.NewReader(data.Encode())

	req := httptest.NewRequest("POST", "/list/", params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	w := httptest.NewRecorder()

	handler.ServeHTTP(w, req)

	resp := w.Result()

	// make sure self crossing polygon returns error.
	if resp.StatusCode != 200 {
		t.Errorf("geo request dit not result error %s", req.URL)
	}

	if resp.Header.Get("Total-Items") != "5" {
		t.Errorf("geo request count is not 5 but %s", resp.Header.Get("Total-Items"))
	}

}
