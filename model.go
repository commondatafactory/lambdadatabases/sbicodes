/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

        This codebase solves:

	The need to have an blazing fast API on
	a tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

/*
 * Options lists keys in registerReduce.
 * used for suggestions of debugging.
 */
func (r registerReduce) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

/*
 * Options lists keys in registerGroupByFunc.
 *
 */

func (r registerGroupByFunc) Options() string {
	options := []string{}
	for k := range r {
		options = append(options, k)
	}
	return strings.Join(options, ",")
}

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Code         string `json:"code"`
	Omschrijving string `json:"omschrijving"`
	L1           string `json:"l1"`
	L1D          string `json:"l1d"`
	L2           string `json:"l2"`
	L2D          string `json:"l2d"`
	L3           string `json:"l3"`
	L3D          string `json:"l3d"`
	L4           string `json:"l4"`
	L4D          string `json:"l4d"`
	L5           string `json:"l5"`
	L5D          string `json:"l5d"`
	Q1           string `json:"q1"`
	Q2           string `json:"q2"`
	Q3           string `json:"q3"`
	Description  string `json:"description"`
}

type ItemOut struct {
	Code         string `json:"code"`
	Omschrijving string `json:"omschrijving"`
	L1           string `json:"l1"`
	L1D          string `json:"l1d"`
	L2           string `json:"l2"`
	L2D          string `json:"l2d"`
	L3           string `json:"l3"`
	L3D          string `json:"l3d"`
	L4           string `json:"l4"`
	L4D          string `json:"l4d"`
	L5           string `json:"l5"`
	L5D          string `json:"l5d"`
	Q1           string `json:"q1"`
	Q2           string `json:"q2"`
	Q3           string `json:"q3"`
	Description  string `json:"description"`
}

type Item struct {
	Label        int // internal index in ITEMS
	Code         string
	Omschrijving string
	L1           string
	L1D          string
	L2           string
	L2D          string
	L3           string
	L3D          string
	L4           string
	L4D          string
	L5           string
	L5D          string
	Q1           string
	Q2           string
	Q3           string
	Description  string
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	return Item{
		label,
		i.Code,
		i.Omschrijving,
		i.L1,
		i.L1D,
		i.L2,
		i.L2D,
		i.L3,
		i.L3D,
		i.L4,
		i.L4D,
		i.L5,
		i.L5D,
		i.Q1,
		i.Q2,
		i.Q3,
		i.Description,
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast indexed selection
func (i *Item) StoreBitArrayColumns() {

}

func (i Item) Serialize() ItemOut {
	return ItemOut{
		i.Code,
		i.Omschrijving,
		i.L1,
		i.L1D,
		i.L2,
		i.L2D,
		i.L3,
		i.L3D,
		i.L4,
		i.L4D,
		i.L5,
		i.L5D,
		i.Q1,
		i.Q2,
		i.Q3,
		i.Description,
	}
}

func (i ItemIn) Columns() []string {
	return []string{
		"code",
		"omschrijving",
		"l1",
		"l1d",
		"l2",
		"l2d",
		"l3",
		"l3d",
		"l4",
		"l4d",
		"l5",
		"l5d",
		"q1",
		"q2",
		"q3",
		"description",
	}
}

func (i ItemOut) Columns() []string {
	return []string{
		"code",
		"omschrijving",
		"l1",
		"l1d",
		"l2",
		"l2d",
		"l3",
		"l3d",
		"l4",
		"l4d",
		"l5",
		"l5d",
		"q1",
		"q2",
		"q3",
		"description",
	}
}

func (i Item) Row() []string {

	return []string{
		i.Code,
		i.Omschrijving,
		i.L1,
		i.L1D,
		i.L2,
		i.L2D,
		i.L3,
		i.L3D,
		i.L4,
		i.L4D,
		i.L5,
		i.L5D,
		i.Q1,
		i.Q2,
		i.Q3,
		i.Description,
	}
}

func (i Item) GetIndex() string {
	return GettersCode(&i)
}

func (i Item) GetGeometry() string {
	return ""
}

// contain filter Code
func FilterCodeContains(i *Item, s string) bool {
	return strings.Contains(i.Code, s)
}

// startswith filter Code
func FilterCodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Code, s)
}

// match filters Code
func FilterCodeMatch(i *Item, s string) bool {
	return i.Code == s
}

// gte filters Code
func FilterCodegte(i *Item, s string) bool {
	return i.Code <= s
}

// lte filters Code
func FilterCodelte(i *Item, s string) bool {
	return i.Code <= s
}

// getter Code
func GettersCode(i *Item) string {
	return i.Code
}

// contain filter Omschrijving
func FilterOmschrijvingContains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.Omschrijving), s)
}

// startswith filter Omschrijving
func FilterOmschrijvingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(strings.ToLower(i.Omschrijving), s)
}

// match filters Omschrijving
func FilterOmschrijvingMatch(i *Item, s string) bool {
	return i.Omschrijving == s
}

// gte filters Omschrijving
func FilterOmschrijvinggte(i *Item, s string) bool {
	return i.Omschrijving <= s
}

// lte filters Omschrijving
func FilterOmschrijvinglte(i *Item, s string) bool {
	return i.Omschrijving <= s
}

// getter Omschrijving
func GettersOmschrijving(i *Item) string {
	return i.Omschrijving
}

// contain filter L1
func FilterL1Contains(i *Item, s string) bool {
	return strings.Contains(i.L1, s)
}

// startswith filter L1
func FilterL1StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L1, s)
}

// match filters L1
func FilterL1Match(i *Item, s string) bool {
	return i.L1 == s
}

// gte filters L1
func FilterL1gte(i *Item, s string) bool {
	return i.L1 <= s
}

// lte filters L1
func FilterL1lte(i *Item, s string) bool {
	return i.L1 <= s
}

// getter L1
func GettersL1(i *Item) string {
	return i.L1
}

// contain filter L1D
func FilterL1DContains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.L1D), s)
}

// startswith filter L1D
func FilterL1DStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L1D, s)
}

// match filters L1D
func FilterL1DMatch(i *Item, s string) bool {
	return i.L1D == s
}

// gte filters L1D
func FilterL1Dgte(i *Item, s string) bool {
	return i.L1D <= s
}

// lte filters L1D
func FilterL1Dlte(i *Item, s string) bool {
	return i.L1D <= s
}

// getter L1D
func GettersL1D(i *Item) string {
	return i.L1D
}

// contain filter L2
func FilterL2Contains(i *Item, s string) bool {
	return strings.Contains(i.L2, s)
}

// startswith filter L2
func FilterL2StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L2, s)
}

// match filters L2
func FilterL2Match(i *Item, s string) bool {
	return i.L2 == s
}

// gte filters L2
func FilterL2gte(i *Item, s string) bool {
	return i.L2 <= s
}

// lte filters L2
func FilterL2lte(i *Item, s string) bool {
	return i.L2 <= s
}

// getter L2
func GettersL2(i *Item) string {
	return i.L2
}

// contain filter L2D
func FilterL2DContains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.L2D), s)
}

// startswith filter L2D
func FilterL2DStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L2D, s)
}

// match filters L2D
func FilterL2DMatch(i *Item, s string) bool {
	return i.L2D == s
}

// gte filters L2D
func FilterL2Dgte(i *Item, s string) bool {
	return i.L2D <= s
}

// lte filters L2D
func FilterL2Dlte(i *Item, s string) bool {
	return i.L2D <= s
}

// getter L2D
func GettersL2D(i *Item) string {
	return i.L2D
}

// contain filter L3
func FilterL3Contains(i *Item, s string) bool {
	return strings.Contains(i.L3, s)
}

// startswith filter L3
func FilterL3StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L3, s)
}

// match filters L3
func FilterL3Match(i *Item, s string) bool {
	return i.L3 == s
}

// gte filters L3
func FilterL3gte(i *Item, s string) bool {
	return i.L3 <= s
}

// lte filters L3
func FilterL3lte(i *Item, s string) bool {
	return i.L3 <= s
}

// getter L3
func GettersL3(i *Item) string {
	return i.L3
}

// contain filter L3D
func FilterL3DContains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.L3D), s)
}

// startswith filter L3D
func FilterL3DStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L3D, s)
}

// match filters L3D
func FilterL3DMatch(i *Item, s string) bool {
	return i.L3D == s
}

// gte filters L3D
func FilterL3Dgte(i *Item, s string) bool {
	return i.L3D <= s
}

// lte filters L3D
func FilterL3Dlte(i *Item, s string) bool {
	return i.L3D <= s
}

// getter L3D
func GettersL3D(i *Item) string {
	return i.L3D
}

// contain filter L4
func FilterL4Contains(i *Item, s string) bool {
	return strings.Contains(i.L4, s)
}

// startswith filter L4
func FilterL4StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L4, s)
}

// match filters L4
func FilterL4Match(i *Item, s string) bool {
	return i.L4 == s
}

// gte filters L4
func FilterL4gte(i *Item, s string) bool {
	return i.L4 <= s
}

// lte filters L4
func FilterL4lte(i *Item, s string) bool {
	return i.L4 <= s
}

// getter L4
func GettersL4(i *Item) string {
	return i.L4
}

// contain filter L4D
func FilterL4DContains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.L4D), s)
}

// startswith filter L4D
func FilterL4DStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L4D, s)
}

// match filters L4D
func FilterL4DMatch(i *Item, s string) bool {
	return i.L4D == s
}

// gte filters L4D
func FilterL4Dgte(i *Item, s string) bool {
	return i.L4D <= s
}

// lte filters L4D
func FilterL4Dlte(i *Item, s string) bool {
	return i.L4D <= s
}

// getter L4D
func GettersL4D(i *Item) string {
	return i.L4D
}

// contain filter L5
func FilterL5Contains(i *Item, s string) bool {
	return strings.Contains(i.L5, s)
}

// startswith filter L5
func FilterL5StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L5, s)
}

// match filters L5
func FilterL5Match(i *Item, s string) bool {
	return i.L5 == s
}

// gte filters L5
func FilterL5gte(i *Item, s string) bool {
	return i.L5 <= s
}

// lte filters L5
func FilterL5lte(i *Item, s string) bool {
	return i.L5 <= s
}

// getter L5
func GettersL5(i *Item) string {
	return i.L5
}

// contain filter L5D
func FilterL5DContains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.L5D), s)
}

// startswith filter L5D
func FilterL5DStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.L5D, s)
}

// match filters L5D
func FilterL5DMatch(i *Item, s string) bool {
	return i.L5D == s
}

// gte filters L5D
func FilterL5Dgte(i *Item, s string) bool {
	return i.L5D <= s
}

// lte filters L5D
func FilterL5Dlte(i *Item, s string) bool {
	return i.L5D <= s
}

// getter L5D
func GettersL5D(i *Item) string {
	return i.L5D
}

// contain filter Q1
func FilterQ1Contains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.Q1), s)
}

// startswith filter Q1
func FilterQ1StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Q1, s)
}

// match filters Q1
func FilterQ1Match(i *Item, s string) bool {
	return i.Q1 == s
}

// gte filters Q1
func FilterQ1gte(i *Item, s string) bool {
	return i.Q1 <= s
}

// lte filters Q1
func FilterQ1lte(i *Item, s string) bool {
	return i.Q1 <= s
}

// getter Q1
func GettersQ1(i *Item) string {
	return i.Q1
}

// contain filter Q2
func FilterQ2Contains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.Q2), s)
}

// startswith filter Q2
func FilterQ2StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Q2, s)
}

// match filters Q2
func FilterQ2Match(i *Item, s string) bool {
	return i.Q2 == s
}

// gte filters Q2
func FilterQ2gte(i *Item, s string) bool {
	return i.Q2 <= s
}

// lte filters Q2
func FilterQ2lte(i *Item, s string) bool {
	return i.Q2 <= s
}

// getter Q2
func GettersQ2(i *Item) string {
	return i.Q2
}

// contain filter Q3
func FilterQ3Contains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.Q3), s)
}

// startswith filter Q3
func FilterQ3StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Q3, s)
}

// match filters Q3
func FilterQ3Match(i *Item, s string) bool {
	return i.Q3 == s
}

// gte filters Q3
func FilterQ3gte(i *Item, s string) bool {
	return i.Q3 <= s
}

// lte filters Q3
func FilterQ3lte(i *Item, s string) bool {
	return i.Q3 <= s
}

// getter Q3
func GettersQ3(i *Item) string {
	return i.Q3
}

// contain filter Description
func FilterDescriptionContains(i *Item, s string) bool {
	return strings.Contains(strings.ToLower(i.Description), s)
}

// startswith filter Description
func FilterDescriptionStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Description, s)
}

// match filters Description
func FilterDescriptionMatch(i *Item, s string) bool {
	return i.Description == s
}

// gte filters Description
func FilterDescriptiongte(i *Item, s string) bool {
	return i.Description <= s
}

// lte filters Description
func FilterDescriptionlte(i *Item, s string) bool {
	return i.Description <= s
}

// getter Description
func GettersDescription(i *Item) string {
	return i.Description
}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			filterKey := f + "-" + c
			if _, ok := RegisterFuncMap[filterKey]; !ok {
				fmt.Println("missing in RegisterMap: " + filterKey)
			}
		}
	}
}

func FilterGeneralStartsWith(i *Item, s string) bool {
	s = strings.ToLower(s)
	return FilterCodeStartsWith(i, s) ||
		FilterOmschrijvingContains(i, s) ||
		FilterL1StartsWith(i, s) ||
		FilterL1DContains(i, s) ||
		FilterL2StartsWith(i, s) ||
		FilterL2DContains(i, s) ||
		FilterL3StartsWith(i, s) ||
		FilterL3DContains(i, s) ||
		FilterL4StartsWith(i, s) ||
		FilterL4DContains(i, s) ||
		FilterL5StartsWith(i, s) ||
		FilterL5DContains(i, s) ||
		FilterQ1Contains(i, s) ||
		FilterQ2Contains(i, s) ||
		FilterQ3Contains(i, s) ||
		FilterDescriptionContains(i, s)

}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	//RegisterFuncMap["search"] = 'EDITYOURSELF'
	RegisterFuncMap["search"] = FilterGeneralStartsWith

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	// example RegisterGetters["value"] = GettersEkey

	// register filters

	//register filters for Code
	RegisterFuncMap["match-code"] = FilterCodeMatch
	RegisterFuncMap["contains-code"] = FilterCodeContains
	RegisterFuncMap["startswith-code"] = FilterCodeStartsWith
	RegisterFuncMap["gte-code"] = FilterCodegte
	RegisterFuncMap["lte-code"] = FilterCodelte
	RegisterGetters["code"] = GettersCode
	RegisterGroupBy["code"] = GettersCode

	//register filters for Omschrijving
	RegisterFuncMap["match-omschrijving"] = FilterOmschrijvingMatch
	RegisterFuncMap["contains-omschrijving"] = FilterOmschrijvingContains
	RegisterFuncMap["startswith-omschrijving"] = FilterOmschrijvingStartsWith
	RegisterFuncMap["gte-omschrijving"] = FilterOmschrijvinggte
	RegisterFuncMap["lte-omschrijving"] = FilterOmschrijvinglte
	RegisterGetters["omschrijving"] = GettersOmschrijving
	RegisterGroupBy["omschrijving"] = GettersOmschrijving

	//register filters for L1
	RegisterFuncMap["match-l1"] = FilterL1Match
	RegisterFuncMap["contains-l1"] = FilterL1Contains
	RegisterFuncMap["startswith-l1"] = FilterL1StartsWith
	RegisterFuncMap["gte-l1"] = FilterL1gte
	RegisterFuncMap["lte-l1"] = FilterL1lte
	RegisterGetters["l1"] = GettersL1
	RegisterGroupBy["l1"] = GettersL1

	//register filters for L1D
	RegisterFuncMap["match-l1d"] = FilterL1DMatch
	RegisterFuncMap["contains-l1d"] = FilterL1DContains
	RegisterFuncMap["startswith-l1d"] = FilterL1DStartsWith
	RegisterFuncMap["gte-l1d"] = FilterL1Dgte
	RegisterFuncMap["lte-l1d"] = FilterL1Dlte
	RegisterGetters["l1d"] = GettersL1D
	RegisterGroupBy["l1d"] = GettersL1D

	//register filters for L2
	RegisterFuncMap["match-l2"] = FilterL2Match
	RegisterFuncMap["contains-l2"] = FilterL2Contains
	RegisterFuncMap["startswith-l2"] = FilterL2StartsWith
	RegisterFuncMap["gte-l2"] = FilterL2gte
	RegisterFuncMap["lte-l2"] = FilterL2lte
	RegisterGetters["l2"] = GettersL2
	RegisterGroupBy["l2"] = GettersL2

	//register filters for L2D
	RegisterFuncMap["match-l2d"] = FilterL2DMatch
	RegisterFuncMap["contains-l2d"] = FilterL2DContains
	RegisterFuncMap["startswith-l2d"] = FilterL2DStartsWith
	RegisterFuncMap["gte-l2d"] = FilterL2Dgte
	RegisterFuncMap["lte-l2d"] = FilterL2Dlte
	RegisterGetters["l2d"] = GettersL2D
	RegisterGroupBy["l2d"] = GettersL2D

	//register filters for L3
	RegisterFuncMap["match-l3"] = FilterL3Match
	RegisterFuncMap["contains-l3"] = FilterL3Contains
	RegisterFuncMap["startswith-l3"] = FilterL3StartsWith
	RegisterFuncMap["gte-l3"] = FilterL3gte
	RegisterFuncMap["lte-l3"] = FilterL3lte
	RegisterGetters["l3"] = GettersL3
	RegisterGroupBy["l3"] = GettersL3

	//register filters for L3D
	RegisterFuncMap["match-l3d"] = FilterL3DMatch
	RegisterFuncMap["contains-l3d"] = FilterL3DContains
	RegisterFuncMap["startswith-l3d"] = FilterL3DStartsWith
	RegisterFuncMap["gte-l3d"] = FilterL3Dgte
	RegisterFuncMap["lte-l3d"] = FilterL3Dlte
	RegisterGetters["l3d"] = GettersL3D
	RegisterGroupBy["l3d"] = GettersL3D

	//register filters for L4
	RegisterFuncMap["match-l4"] = FilterL4Match
	RegisterFuncMap["contains-l4"] = FilterL4Contains
	RegisterFuncMap["startswith-l4"] = FilterL4StartsWith
	RegisterFuncMap["gte-l4"] = FilterL4gte
	RegisterFuncMap["lte-l4"] = FilterL4lte
	RegisterGetters["l4"] = GettersL4
	RegisterGroupBy["l4"] = GettersL4

	//register filters for L4D
	RegisterFuncMap["match-l4d"] = FilterL4DMatch
	RegisterFuncMap["contains-l4d"] = FilterL4DContains
	RegisterFuncMap["startswith-l4d"] = FilterL4DStartsWith
	RegisterFuncMap["gte-l4d"] = FilterL4Dgte
	RegisterFuncMap["lte-l4d"] = FilterL4Dlte
	RegisterGetters["l4d"] = GettersL4D
	RegisterGroupBy["l4d"] = GettersL4D

	//register filters for L5
	RegisterFuncMap["match-l5"] = FilterL5Match
	RegisterFuncMap["contains-l5"] = FilterL5Contains
	RegisterFuncMap["startswith-l5"] = FilterL5StartsWith
	RegisterFuncMap["gte-l5"] = FilterL5gte
	RegisterFuncMap["lte-l5"] = FilterL5lte
	RegisterGetters["l5"] = GettersL5
	RegisterGroupBy["l5"] = GettersL5

	//register filters for L5D
	RegisterFuncMap["match-l5d"] = FilterL5DMatch
	RegisterFuncMap["contains-l5d"] = FilterL5DContains
	RegisterFuncMap["startswith-l5d"] = FilterL5DStartsWith
	RegisterFuncMap["gte-l5d"] = FilterL5Dgte
	RegisterFuncMap["lte-l5d"] = FilterL5Dlte
	RegisterGetters["l5d"] = GettersL5D
	RegisterGroupBy["l5d"] = GettersL5D

	//register filters for Q1
	RegisterFuncMap["match-q1"] = FilterQ1Match
	RegisterFuncMap["contains-q1"] = FilterQ1Contains
	RegisterFuncMap["startswith-q1"] = FilterQ1StartsWith
	RegisterFuncMap["gte-q1"] = FilterQ1gte
	RegisterFuncMap["lte-q1"] = FilterQ1lte
	RegisterGetters["q1"] = GettersQ1
	RegisterGroupBy["q1"] = GettersQ1

	//register filters for Q2
	RegisterFuncMap["match-q2"] = FilterQ2Match
	RegisterFuncMap["contains-q2"] = FilterQ2Contains
	RegisterFuncMap["startswith-q2"] = FilterQ2StartsWith
	RegisterFuncMap["gte-q2"] = FilterQ2gte
	RegisterFuncMap["lte-q2"] = FilterQ2lte
	RegisterGetters["q2"] = GettersQ2
	RegisterGroupBy["q2"] = GettersQ2

	//register filters for Q3
	RegisterFuncMap["match-q3"] = FilterQ3Match
	RegisterFuncMap["contains-q3"] = FilterQ3Contains
	RegisterFuncMap["startswith-q3"] = FilterQ3StartsWith
	RegisterFuncMap["gte-q3"] = FilterQ3gte
	RegisterFuncMap["lte-q3"] = FilterQ3lte
	RegisterGetters["q3"] = GettersQ3
	RegisterGroupBy["q3"] = GettersQ3

	//register filters for Description
	RegisterFuncMap["match-description"] = FilterDescriptionMatch
	RegisterFuncMap["contains-description"] = FilterDescriptionContains
	RegisterFuncMap["startswith-description"] = FilterDescriptionStartsWith
	RegisterFuncMap["gte-description"] = FilterDescriptiongte
	RegisterFuncMap["lte-description"] = FilterDescriptionlte
	RegisterGetters["description"] = GettersDescription
	RegisterGroupBy["description"] = GettersDescription

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount

	// custom
}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"code":  func(i, j int) bool { return items[i].Code < items[j].Code },
		"-code": func(i, j int) bool { return items[i].Code > items[j].Code },

		"omschrijving":  func(i, j int) bool { return items[i].Omschrijving < items[j].Omschrijving },
		"-omschrijving": func(i, j int) bool { return items[i].Omschrijving > items[j].Omschrijving },

		"l1":  func(i, j int) bool { return items[i].L1 < items[j].L1 },
		"-l1": func(i, j int) bool { return items[i].L1 > items[j].L1 },

		"l1d":  func(i, j int) bool { return items[i].L1D < items[j].L1D },
		"-l1d": func(i, j int) bool { return items[i].L1D > items[j].L1D },

		"l2":  func(i, j int) bool { return items[i].L2 < items[j].L2 },
		"-l2": func(i, j int) bool { return items[i].L2 > items[j].L2 },

		"l2d":  func(i, j int) bool { return items[i].L2D < items[j].L2D },
		"-l2d": func(i, j int) bool { return items[i].L2D > items[j].L2D },

		"l3":  func(i, j int) bool { return items[i].L3 < items[j].L3 },
		"-l3": func(i, j int) bool { return items[i].L3 > items[j].L3 },

		"l3d":  func(i, j int) bool { return items[i].L3D < items[j].L3D },
		"-l3d": func(i, j int) bool { return items[i].L3D > items[j].L3D },

		"l4":  func(i, j int) bool { return items[i].L4 < items[j].L4 },
		"-l4": func(i, j int) bool { return items[i].L4 > items[j].L4 },

		"l4d":  func(i, j int) bool { return items[i].L4D < items[j].L4D },
		"-l4d": func(i, j int) bool { return items[i].L4D > items[j].L4D },

		"l5":  func(i, j int) bool { return items[i].L5 < items[j].L5 },
		"-l5": func(i, j int) bool { return items[i].L5 > items[j].L5 },

		"l5d":  func(i, j int) bool { return items[i].L5D < items[j].L5D },
		"-l5d": func(i, j int) bool { return items[i].L5D > items[j].L5D },

		"q1":  func(i, j int) bool { return items[i].Q1 < items[j].Q1 },
		"-q1": func(i, j int) bool { return items[i].Q1 > items[j].Q1 },

		"q2":  func(i, j int) bool { return items[i].Q2 < items[j].Q2 },
		"-q2": func(i, j int) bool { return items[i].Q2 > items[j].Q2 },

		"q3":  func(i, j int) bool { return items[i].Q3 < items[j].Q3 },
		"-q3": func(i, j int) bool { return items[i].Q3 > items[j].Q3 },

		"description":  func(i, j int) bool { return items[i].Description < items[j].Description },
		"-description": func(i, j int) bool { return items[i].Description > items[j].Description },
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
