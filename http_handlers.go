package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"net/http"
	"runtime"
	"sort"
	"strconv"
	"sync"
	"time"

	. "github.com/Attumm/settingo/settingo"

	"github.com/goccy/go-json"
	//
	// goatcounter "zgo.at/goatcounter/handlers"
)

// API list headers
func setHeader(items Items, w http.ResponseWriter, query Query, queryTime int64) {

	headerData := getHeaderData(items, query, queryTime)

	for key, val := range headerData {
		w.Header().Set(key, val)
	}

	filtersKey, err := query.CacheKey()
	downloadFilename := "items.csv"
	if err == nil {
		downloadFilename = fmt.Sprintf("sbi_data-%s", filtersKey)
	}

	if query.ReturnFormat == "csv" {
		fn := fmt.Sprintf("attachment; filename=\"%s\"", downloadFilename)
		w.Header().Set("Content-Disposition", fn)
		w.Header().Set("Content-Type", "text/csv; charset=utf-8")
	} else {
		w.Header().Set("Content-Type", "application/json")
	}

	if len(items) == 0 {
		w.WriteHeader(http.StatusNotFound)
	}
}

// Handle API request Errors log the query in the headers for debugging.
func handleQueryError(err error, query Query, w http.ResponseWriter) {
	response := make(map[string]string)
	bytesQuery, _ := json.Marshal(query)
	w.Header().Set("Query", string(bytesQuery))
	response["error"] = err.Error()
	w.WriteHeader(500)
	err = json.NewEncoder(w).Encode(response)
	if err != nil {
		log.Println(err)
	}
}

type ReduceResult map[string]string
type GroupByResult map[string]ReduceResult

var GroupByBodyCache = make(map[string]GroupByResult)
var GroupByHeaderCache = make(map[string]HeaderData)

var cacheLock = sync.RWMutex{}

func logRequest(r *http.Request, itemsCount int, queryTime int64, query Query, err error) {
	msg := fmt.Sprint(
		"total: ", len(ITEMS),
		" hits: ", itemsCount,
		" time: ", queryTime, "ms ",
		" early exit: ", query.EarlyExit(),
		"; geoGiven?: ", query.GeometryGiven,
		" url: ", r.URL,
		"; error: ", err,
	)

	log.Printf(NoticeColorN, msg)
}

// giveCachedResponse try to find repsonse in cache (groupby only)
func giveCachedResponse(w http.ResponseWriter, r *http.Request, query Query) bool {
	cacheKey, err := query.CacheKey()

	if err != nil {
		return false
	}

	if len(query.GroupBy) == 0 || len(query.Reduce) == 0 {
		return false
	}

	// lets check the cache.
	cacheLock.RLock()
	groupByResult, found := GroupByBodyCache[cacheKey]
	headerCache := GroupByHeaderCache[cacheKey]
	cacheLock.RUnlock()

	if found {
		w.Header().Set("Content-Type", "application/json")

		for key, val := range headerCache {
			w.Header().Set(key, val)
		}
		w.Header().Set("used-cache", "yes")
		json.NewEncoder(w).Encode(groupByResult)
		msg := fmt.Sprintf("cached response key: %s", cacheKey)
		log.Printf(GreenColorN, msg)
		return found
	}

	return false
}

func contextListRest(itemChan ItemsChannel, operations GroupedOperations) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		query, err := parseURLParameters(r)

		if err != nil {
			handleQueryError(err, query, w)
			return
		}

		if giveCachedResponse(w, r, query) {
			return
		}

		items, queryTime, err := runQuery(&ITEMS, query, operations)

		logRequest(r, len(items), queryTime, query, err)

		if err != nil {
			handleQueryError(err, query, w)
			return
		}

		setHeader(items, w, query, queryTime)

		// We want to return all/filtered items.
		// and we do not have a groupby
		if !query.GroupByGiven && query.ReduceGiven {
			reduceOnlyResponse(w, items, query, operations)
			return
		}

		/*
		 * groupby items on column
		 */

		if query.GroupByGiven {
			groupByResponse(w, items, query, operations)
			return
		}

		/*
		 *  default return a list of items.
		 *  lmited.
		 */

		if len(items) > 1000000 {
			msg := "Use Pagination page & pagesize"
			err = errors.New(msg)
			handleQueryError(err, query, w)
			return
		}

		if query.EarlyExit() {
			items = sortLimit(items, query)
		}

		if query.ReturnFormat == "csv" {
			writeCSV(items, w)
		} else {
			err = json.NewEncoder(w).Encode(items)
			if err != nil {
				log.Printf(ErrorColorN, err)
			}
		}

		// force nil helps garbage collection
		items = nil
	}
}

// reduceOnlyResponse
func reduceOnlyResponse(w http.ResponseWriter, items Items, query Query, operations GroupedOperations) {
	reduceFunc := operations.Reduce[query.Reduce]
	result := reduceFunc(items)
	err := json.NewEncoder(w).Encode(result)
	if err != nil {
		log.Printf(ErrorColorN, err)
	}
	items = nil
}

// groupByResponse return Grouped By Response with a optional Reduce
func groupByResponse(w http.ResponseWriter, items Items, query Query, operations GroupedOperations) {

	groupByItems := groupByRunner(items, query.GroupBy)
	items = nil

	if query.ReduceGiven {
		result := make(GroupByResult)
		reduceFunc := operations.Reduce[query.Reduce]

		for key, val := range groupByItems {
			result[key] = reduceFunc(val)
		}

		groupByItems = nil

		if len(result) == 0 {
			return
		}

		// do not cache large objects
		if len(result) < 100 {
			// Cache group-by reduce repsonse
			cacheLock.Lock()
			cacheKey, _ := query.CacheKey()
			GroupByBodyCache[cacheKey] = result
			headerData := getHeaderData(items, query, 0)
			GroupByHeaderCache[cacheKey] = headerData
			cacheLock.Unlock()
			log.Printf("cached %s", cacheKey)
		} else {
			log.Printf("to big to cache")
		}
		json.NewEncoder(w).Encode(result)
		return
	}
	json.NewEncoder(w).Encode(groupByItems)
}

func contextAddRest(itemChan ItemsChannel, operations GroupedOperations) func(http.ResponseWriter, *http.Request) {

	return func(w http.ResponseWriter, r *http.Request) {
		jsonDecoder := json.NewDecoder(r.Body)
		var items ItemsIn
		err := jsonDecoder.Decode(&items)
		if err != nil {
			fmt.Println(err)
			w.WriteHeader(500)
			return
		}
		msg := fmt.Sprint("adding ", len(items))
		fmt.Printf(WarningColorN, msg)

		strictMode := SETTINGS.GetBool("strict-mode")
		for n, item := range items {

			if (*item == ItemIn{}) {
				fmt.Printf("unable to process item %d of batch\n", n)
				if strictMode {
					fmt.Printf("strict mode stopping ingestion of batch\n")
					w.WriteHeader(406)
					return
				}
			}
		}
		itemChan <- items
		w.WriteHeader(204)
	}
}

func rmRest(w http.ResponseWriter, r *http.Request) {
	ITEMS = make(Items, 0, 100*1000)
	msg := "removed items from database"
	fmt.Printf(WarningColorN, msg)
	ITEMS = Items{}

	go func() {
		time.Sleep(1 * time.Second)
		runtime.GC()
	}()
	w.WriteHeader(204)
}

func writeCSV(items Items, w http.ResponseWriter) {
	writer := csv.NewWriter(w)

	columns := ItemOut{}.Columns()
	writer.Comma = ';'
	writer.Write(columns)
	writer.Flush()

	for i := range items {
		writer.Write(items[i].Row())
		writer.Flush()
	}
}

func loadRest(w http.ResponseWriter, r *http.Request) {
	storagename, _, retrievefunc, filename := handleInputStorage(r)

	start := time.Now()
	msg := fmt.Sprintf("retrieving with: %s, with filename: %s", storagename, filename)
	fmt.Printf(WarningColorN, msg)
	itemsAdded, err := retrievefunc(filename)
	diff := time.Since(start)
	msg = fmt.Sprint("loading time: ", diff)
	fmt.Printf(WarningColorN, msg)

	if err != nil {
		log.Printf("could not open %s reason %s", filename, err)
		w.Write([]byte("500 - could not load data"))
	}

	msg = fmt.Sprint("Loaded new items in memory amount: ", itemsAdded)
	fmt.Printf(WarningColorN, msg)
}

func handleInputStorage(r *http.Request) (string, storageFunc, retrieveFunc, string) {
	urlPath := r.URL.Path
	storagename := SETTINGS.Get("STORAGEMETHOD")

	if len(urlPath) > len("/mgmt/save/") {
		storagename = urlPath[len("/mgmt/save/"):]
	}
	storagefunc, found := STORAGEFUNCS[storagename]
	if !found {
		storagename := SETTINGS.Get("STORAGEMETHOD")
		storagefunc = STORAGEFUNCS[storagename]
	}

	retrievefunc, found := RETRIEVEFUNCS[storagename]
	if !found {
		storagename := SETTINGS.Get("STORAGEMETHOD")
		retrievefunc = RETRIEVEFUNCS[storagename]
	}

	filename := fmt.Sprintf("%s.%s", FILENAME, storagename)

	return storagename, storagefunc, retrievefunc, filename
}

func saveRest(w http.ResponseWriter, r *http.Request) {
	msg := fmt.Sprintf("storing items %d", len(ITEMS))
	fmt.Printf(WarningColor, msg)

	fmt.Println("full", r.URL.Path)

	storagename, storagefunc, _, filename := handleInputStorage(r)
	msg = fmt.Sprintf("storage method: %s filename: %s\n", storagename, filename)
	fmt.Printf(WarningColor, msg)

	size, err := storagefunc(filename)
	if err != nil {
		fmt.Println("unable to write file reason:", err)
		w.WriteHeader(500)
		return
	}
	msg = fmt.Sprintf("filename %s, filesize: %d mb\n", filename, size/1024/1025)
	fmt.Printf(WarningColor, msg)

	w.WriteHeader(204)
}

type ShowItem struct {
	IsShow bool   `json:"isShow"`
	Label  string `json:"label"`
	Name   string `json:"name"`
}

type Meta struct {
	Fields []ShowItem `json:"fields"`
	View   string     `json:"view"`
}

type searchResponse struct {
	Count int      `json:"count"`
	Data  ItemsOut `json:"data"`
	MMeta *Meta    `json:"meta"`
}

func outputItems(items Items) ItemsOut {

	itemsout := make(ItemsOut, 0, len(items))

	for _, oneitem := range items {
		orgItem := oneitem.Serialize()
		itemsout = append(itemsout, &orgItem)
	}

	return itemsout
}

func makeResp(items Items) searchResponse {

	itemsout := outputItems(items)

	fields := []ShowItem{}
	columns := ItemOut{}.Columns()
	for _, column := range columns {
		fields = append(fields, ShowItem{IsShow: true, Name: column, Label: column})
	}

	return searchResponse{
		Count: len(items),
		Data:  itemsout,
		MMeta: &Meta{Fields: fields, View: "table"},
	}
}

func allowedMethod(w http.ResponseWriter, r *http.Request) bool {
	// Define the list of allowed HTTP methods
	allowedMethods := map[string]bool{
		http.MethodGet:  true,
		http.MethodHead: true,
		http.MethodPost: true,
		// http.MethodPut:    true,
		// http.MethodPatch:  true,
		// http.MethodDelete: true,
		http.MethodConnect: true,
		http.MethodOptions: true,
		http.MethodTrace:   true,
	}

	// Check if the request's method is allowed
	if !allowedMethods[r.Method] {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return false
	}
	return true
}

func corsEnabled(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		origin := "" // r.Header.Get("Origin")
		if origin == "" {
			origin = "*"
		}
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Expose-Headers", "Page, Page-Size, Total-Pages, Query, Total-Items, Query-Duration, Content-Type, X-CSRF-Token, Authorization")
		w.Header().Set("API-Version", APIVERSION)

		if !allowedMethod(w, r) {
			return
		}

		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Allow-Methods", "GET,POST")
			w.Header().Set("Access-Control-Expose-Headers", "API-Version, Page, Page-Size, Total-Pages, Query, Total-Items, Query-Duration, Content-Disposition, Content-Type, X-CSRF-Token, Authorization")
			w.Header().Set("Access-Control-Allow-Headers", "Page, Page-Size, Total-Pages, Query, Total-Items, Query-Duration, Content-Disposition, Content-Type, X-CSRF-Token, Authorization")
			return
		} else {
			// make sure items are not being modified during request
			// otherwise wait..
			lock.RLock()
			defer lock.RUnlock()
			h.ServeHTTP(w, r)
		}
	})

}

func passThrough(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// make sure items are not being modified during request
		// otherwise wait..
		lock.RLock()
		defer lock.RUnlock()
		h.ServeHTTP(w, r)
	})
}

func MIDDLEWARE(cors bool) func(http.Handler) http.Handler {

	if cors {
		return corsEnabled
	}

	return passThrough
}

func contextSearchRest(itemChan ItemsChannel, operations GroupedOperations) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query, err := parseURLParameters(r)
		if err != nil {
			handleQueryError(err, query, w)
			return
		}

		// always use pagination using search
		query.PageGiven = true

		// set a default limit on search queries
		if !query.LimitGiven {
			query.LimitGiven = true
			query.Limit = 1000
		}

		items, queryTime, err := runQuery(&ITEMS, query, operations)

		if err != nil {
			handleQueryError(err, query, w)
			return
		}

		if len(items) == 0 {
			w.WriteHeader(404)
			w.Write([]byte("nothing found"))
			return
		}

		logRequest(r, len(items), queryTime, query, err)
		headerData := getHeaderData(items, query, queryTime)

		if !query.EarlyExit() {
			items = sortLimit(items, query)
		}

		w.Header().Set("Content-Type", "application/json")

		for key, val := range headerData {
			w.Header().Set(key, val)
		}
		if len(items) == 0 {
			w.WriteHeader(204)
			return
		}

		response := makeResp(items)
		json.NewEncoder(w).Encode(response)
		items = nil
	}
}

func contextTypeAheadRest(itemChan ItemsChannel, operations GroupedOperations) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query, err := parseURLParameters(r)
		if err != nil {
			handleQueryError(err, query, w)
			return
		}

		column := r.URL.Path[len("/typeahead/"):]
		if column[len(column)-1] == '/' {
			column = column[:len(column)-1]
		}

		/*
			if _, ok := operations.Getters[column]; !ok {
				w.WriteHeader(404)
				w.Write([]byte("wrong column name"))
				return
			}
		*/

		results, queryTime := runTypeAheadQuery(&ITEMS, column, query, operations)
		if len(results) == 0 {
			w.WriteHeader(404)
			w.Write([]byte("nothing found"))
			return
		}

		logRequest(r, len(results), queryTime, query, err)

		headerData := getHeaderDataSlice(results, query, queryTime)

		w.Header().Set("Content-Type", "application/json")
		for key, val := range headerData {
			w.Header().Set(key, val)
		}

		json.NewEncoder(w).Encode(results)
		results = nil
	}
}

func helpRest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	response := make(map[string][]string)
	registeredFilters := []string{}
	for k := range RegisterFuncMap {
		registeredFilters = append(registeredFilters, k)
	}

	registeredExcludes := []string{}
	for k := range RegisterFuncMap {
		registeredExcludes = append(registeredExcludes, "!"+k)
	}

	registeredAnys := []string{}
	// TODO create const for the "any_" or exclude prefix
	for k := range RegisterFuncMap {
		registeredAnys = append(registeredAnys, "any_"+k)
	}

	registeredGroupbys := []string{}
	for k := range RegisterGroupBy {
		registeredGroupbys = append(registeredGroupbys, k)
	}

	registerReduces := []string{}
	for k := range RegisterReduce {
		registerReduces = append(registerReduces, k)
	}

	msize := min(10, len(ITEMS))
	newItems := make(Items, msize)
	for i := 0; i < msize; i++ {
		newItems = append(newItems, ITEMS[i])
	}

	_, registeredSortings := sortBy(newItems, []string{})

	sort.Strings(registeredFilters)
	sort.Strings(registeredExcludes)
	sort.Strings(registeredAnys)
	sort.Strings(registeredGroupbys)
	sort.Strings(registeredSortings)
	sort.Strings(registerReduces)

	response["bron_data_dates"] = []string{
		"BAG - kadaster : 2023-05",
		"ENERGIEKLASSES - rvo : 2023-07",
		"WOZ CBS:  2022",
		"EANCODE eancodeboek.nl (gasaansluitingen) : 2022-09",
		"ENERGIEDATA: alle netbeheerders peildatum 01-2023 gegevens over het jaar 2022",
	}

	response["filters"] = registeredFilters
	response["exclude_filters"] = registeredExcludes
	response["anys_filters"] = registeredAnys
	response["groupby"] = registeredGroupbys
	response["sortby"] = registeredSortings
	response["reduce"] = registerReduces

	totalItems := strconv.Itoa(len(ITEMS))

	host := SETTINGS.Get("http_db_host")
	response["total-items"] = []string{totalItems}

	response["settings"] = []string{
		fmt.Sprintf("host: %s", host),
	}
	response["examples"] = []string{
		fmt.Sprintf("search: http://%s/list/?startswith-gemeentenaam=ams&page=1&pagesize=1", host),
		fmt.Sprintf("search with limit: http://%s/list/?page=1&pagesize=10&limit=5", host),
		fmt.Sprintf("sorting: http://%s/list/?straatnaam-startswith=Ams&page=10&pagesize=100&sortby=-country", host),
		fmt.Sprintf("filtering: http://%s/list/?contains=144&contains-case=10&page=1&pagesize=1", host),
		fmt.Sprintf("groupby: http://%s/list/?contains-case=10&groupby=postcode", host),
		fmt.Sprintf("aggregation: http://%s/list/?match-gemeentecode=GM054&groupby=postcode&reduce=count", host),
		fmt.Sprintf("chain the same filters: http://%s/list/?contains-gemeentenaam=dam", host),
		fmt.Sprintf("typeahead use the name of the column in this case straat: http://%s/typeahead/straat/?starts-with=127&limit=15", host),
	}
	json.NewEncoder(w).Encode(response)
}
