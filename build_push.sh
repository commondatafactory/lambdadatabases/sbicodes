#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

REV=$(git rev-parse --short HEAD)
export DOCKER_BUILDKIT=1

#go test

docker build -t registry.gitlab.com/commondatafactory/lambdadatabases/sbicodes:"$REV$1" .
docker push registry.gitlab.com/commondatafactory/lambdadatabases/sbicodes:"$REV$1"
